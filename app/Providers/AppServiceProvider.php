<?php

namespace App\Providers;

use App\Services\Contracts\SubscriptionScheduleServiceContract;
use App\Services\SubscriptionScheduleService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(SubscriptionScheduleServiceContract::class, function($app) {
            return new SubscriptionScheduleService();
        });
    }
}
